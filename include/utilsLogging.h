
#ifndef UTILSLOGGING_H_
#define UTILSLOGGING_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "utils.h"
#include "anyoption.h"

    extern void writeLineOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > lineOutageCounts, std::vector<Line> lines, int numThreads);

    extern void writeGeneratorOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj, bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads);

    extern std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            int numThreads);
                                
    extern std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness,
            double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,int numThreads);

    extern void getTimeStamp(char* aTime);
#endif