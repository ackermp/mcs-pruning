    
#ifndef UTILSCOMMAND_H_
#define UTILSCOMMAND_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "utils.h"
#include "anyoption.h"

    extern void setUsage(AnyOption* opt);
    extern void setOptions(AnyOption* opt);
#endif