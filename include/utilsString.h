
#ifndef UTILSSTRING_H_
#define UTILSSTRING_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "utils.h"
#include "anyoption.h"

    extern std::string vectorToString(std::vector<double> v);
    extern std::string vectorToString(std::vector<int> v);
    extern std::string arrayToString(int* v, int size);
    extern void tokenizeString(std::string str,std::vector<std::string>& tokens,const std::string& delimiter );
#endif