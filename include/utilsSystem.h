    
#ifndef UTILSSYSTEM_H_
#define UTILSSYSTEM_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
    #include "omp.h"
#endif

#include "utils.h"
#include "anyoption.h"

    extern void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem, std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& Buses);
#endif